import matplotlib.pyplot as plt

# 用于存储读取的十六进制数据的列表
hex_data = []

# 打开文件并读取数据
with open('Data.txt', 'r') as file:
    # 读取文件内容
    file_content = file.read()
    # 以分隔符"23"分割数据，并去除空白字符
    data_parts = file_content.split('23')
    data_parts = [part.strip() for part in data_parts if part.strip()]
    
    # 将十六进制字符串转换为整数并存储在hex_data列表中
    for part in data_parts[-600:]:
        hex_data.append(int(part.replace(" ", ""), 16))

# 使用matplotlib绘制数据
plt.plot(hex_data, marker='', linestyle='-', linewidth=1)
plt.xlabel('Index')
plt.ylabel('Hex Value')
plt.title('Hex Data Plot')
plt.grid(True)

# 显示图形
plt.show()

